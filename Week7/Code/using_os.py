#!/usr/bin/env python
""" This scirpt is an illustration of the powers of subprocess to search
files and directories in the home directory."""
__author__ = 'Jack Rowntree jsr217@ic.ac.uk'
__version__ = '0.0.1'


# Hint: look in subprocess.os and/or subprocess.os.path and/or 
# subprocess.os.walk for helpful functions

import subprocess
import re
#################################
#~Get a list of files and 
#~directories in your home/ that start with an uppercase 'C'


# Get the user's home directory.
home = subprocess.os.path.expanduser("~")

# Create a list to store the results.
FilesDirsStartingWithC = []
# Use a for loop to walk through the home directory.
for (dir, subdir, files) in subprocess.os.walk(home):
	if dir.startswith('/home/jack/C'): #if dir starts with[], add dir to list
		FilesDirsStartingWithC.append(dir)	
	for i in subdir:# if subdir starts with [], add dir/subdir to list
		if i.startswith('C'):
			subdirname=dir+'/'+i
			FilesDirsStartingWithC.append(subdirname)
		for j in files: ## if file starts with [], add dir/subdir/file to list
			if i.startswith('C'):
				filename=dir+'/'+i+'/'+j
				FilesDirsStartingWithC.append(filename)
print(FilesDirsStartingWithC) 
	
  
#################################
# Get files and directories in your home/ that start with either an 
# upper or lower case 'C'
# Create a list to store the results.
FilesDirsStartingWith = []
# Use a for loop to walk through the home directory.
for (dir, subdir, files) in subprocess.os.walk(home): 
	if (dir.startswith('/home/jack/C') or  
	dir.startswith('/home/jack/c')):#if dir starts with[], add dir to list
		FilesDirsStartingWithC.append(dir)	
	for i in subdir:# if subdir starts with [], add dir/subdir to list
		if (i.startswith('C') or 
		i.startswith('c')):
			subdirname=dir+'/'+i
			FilesDirsStartingWithC.append(subdirname)
		for j in files:# if file starts with [], add dir/subdir/file to list
			if (i.startswith('C') or 
			i.startswith('c')):
				filename=dir+'/'+i+'/'+j
				FilesDirsStartingWithC.append(filename)
print(FilesDirsStartingWith)
	
#################################
# Get only directories in your home/ that start with either an upper or 
#~lower case 'C' 

FilesStartingWithC = []
# Use a for loop to walk through the home directory.
for (dir, subdir, files) in subprocess.os.walk(home): 
	if (dir.startswith('/home/jack/C') or  
	dir.startswith('/home/jack/c')):#if dir starts with[], add dir to list
		FilesStartingWithC.append(dir)	
	for i in subdir:# if subdir starts with [], add dir/subdir to list
		if (i.startswith('C') or 
		i.startswith('c')):
			subdirname=dir+'/'+i
			FilesStartingWithC.append(subdirname)
		
print(FilesStartingWithC)
