#!/usr/bin/env python

""" This script runs LV1.py and LV2.py using subprocess profiles them with cProfile. """
	
__author__ = 'Jack Rowntree jsr217@ic.ac.uk'
__version__ = '0.0.1'
__date__ = 'Nov 2017'

import cProfile
import LV1
import LV2
import subprocess

########## Subprocessing to run python script and defining it in a function ##########
def runLV1():
	subprocess.os.system("python LV1.py")

def runLV2():
	subprocess.os.system("python LV2.py")

########## Profiling LV scripts ##########
cProfile.run('runLV1()')
cProfile.run('runLV2()')
