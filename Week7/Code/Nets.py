#!/usr/bin/env python

"""Creates a network plot from edge and node data on QMEE PhD collaborations.
Produces a file in """
__author__ = 'Jack Rowntree jsr217@ic.ac.uk'
__version__ = '0.0.1'

import networkx as nx
import scipy as sc 
import matplotlib.pyplot as plt
import numpy as np
import matplotlib.lines as mlines

#close any matplotlib objects, read in edge and node files
plt.close('all')
links=np.loadtxt('../Data/QMEE_Net_Mat_edges.csv', delimiter=',',skiprows=1)
nodes=np.loadtxt('../Data/QMEE_Net_Mat_nodes.csv', delimiter=',',skiprows=1,dtype='str')

inst_type=nodes

#initialise empty graph
G=nx.Graph()    
rows,cols=np.where(links>0)#make 2 arrays of array indexes where links>0
edges=zip(rows.tolist(),cols.tolist())#make array from this data
G.add_edges_from(edges)#add to graph

###create list of weights from the values in the edges table
weights=[]
for u,v,d in G.edges(data=True):
	d['weight']=links[u][v]/10
	weights.append(d['weight'])

#create dictionary of names from nodes table
nodnames=[i[0] for i in nodes]
newnames=dict(zip(G.node, nodnames))

#create category key in G.node dictioanry tuple from node table
i=0
for node in G.nodes():	
	G.node[node]['category'] = inst_type[:,1][i]
	i=i+1

####Create fillcolr key in G.node dictionary tuple- assign category values colors
for node in G.nodes():
	if G.node[node]['category']=='University':
		G.node[node]['fillcolor']='red'
	if G.node[node]['category']=='Non-Hosting Partners':
		G.node[node]['fillcolor']='green'
	if G.node[node]['category']=='Hosting Partner':
		G.node[node]['fillcolor']='blue'
#make list of colors according to fillcolor values		
colors=[]
for node in G.nodes():
	colors.append(G.node[node]['fillcolor'])
	
#make list of sizes from values in node table * 50
sizes=[]
for i in inst_type[:,2]:
	sizes.append(int(i)*50)
	
#draw graph using as arguments the objects I have made	
nx.draw(G, with_labels=True, width=weights, node_color=colors, labels=newnames,node_size=sizes)

#create legend objects
line1 = mlines.Line2D(range(1), range(1), color="white", marker='o', markersize=15, markerfacecolor="red")
line2 = mlines.Line2D(range(1), range(1), color="white", marker='o', markersize=15,markerfacecolor="blue")
line3 = mlines.Line2D(range(1), range(1), color="white", marker='o', markersize=15, markerfacecolor="green")
#plot legend
plt.legend((line1,line2,line3),('University','Hosting Partner', 'Non-Hosting Partner'),numpoints=1, loc='best')
#initialise file location
plt.savefig("../Results/Nets.svg", format="svg")
plt.show()



