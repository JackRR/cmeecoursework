#!/usr/bin/env python

"""Plot a snapshot of a food web network.
Requires: adjacency list of who eats whom(column one consumer name/id,
column 2 resource name/id), list of species names/ids and properties eg. biomass
(node abundance) or average body mass"""
__author__ = 'Jack Rowntree jsr217@ic.ac.uk'
__version__ = '0.0.1'


import networkx as nx
import scipy as sc 
import matplotlib.pyplot as plt

def GenRdmAdjList(N=2, C=0.5):
	"""generate random adjecency list with N nodes and connectance probability C"""
	Ids=range(N)
	ALst=[]
	for i in Ids:
		if sc.random.uniform(0,1,1) < C:
			Lnk=sc.random.choice(Ids,2).tolist()
			if Lnk[0]!=Lnk[1]:
				ALst.append(Lnk)
				
	return ALst
	
##Assign body mass range
SizRan=([-10,10])
			
##Assign number of species MaxN and connectance C
MaxN=30
C=0.75	

#Generate adjacency list:
AdjL=sc.array(GenRdmAdjList(MaxN,C))

#Generate node(species) data:
Sps=sc.unique(AdjL)
Sizs=sc.random.uniform(SizRan[0],SizRan[1],MaxN)

#Plotting

plt.close('all')

#Calculate coordinates for circular config.
pos=nx.circular_layout(Sps)

G=nx.Graph()
G.add_nodes_from(Sps)
G.add_edges_from(tuple(AdjL))
NodSizs=10**-32 + (Sizs-min(Sizs))/(max(Sizs)-min(Sizs))
nx.draw(G, pos, node_size=NodSizs*1000)
plt.show()
