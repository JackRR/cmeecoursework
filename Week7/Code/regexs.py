#!/usr/bin/env python
""" This scirpt is an illustration of the powers of regex in python."""
__author__ = 'Jack Rowntree jsr217@ic.ac.uk'
__version__ = '0.0.1'


import re

my_string="a given string"
#find whitespace in the string
match=re.search(r'\s', my_string)
print (match)
match.group()


match=re.search(r'd', my_string)

print (match)

my_string = 'an example'
match = re.search(r'\w*\s', my_string)
if match:#shows whether pattern was found - nice!
	print ('found a match:', match.group())
else:
	print ('did not find a match')
	
match =re.search(r'\d', "it takes 2 to tango")
print (match.group())

match = re.search(r'\s\w*\s', 'once upon a time')

match=re.search(r'\sw{1,3}\s', 'once upon a time')
if match !=None:
	match.group()

match=re.search(r'\s\w*$', 'once upon a time')
match.group() 

match= re.search(r'\w*\s\d*\d', 'take 2 grams of H2O')
match.group()

match=re.search(r'^\w*.*\s', 'once upon a time')
match.group()

match=re.search(r'^\w*.*?\s', 'once upon a time')
match.group()

match=re.search(r'<.+>', 'This is a <EM>first</EM> test')
match.group()#greeedyyyyyyy

match=re.search(r'<.+?>', 'This is a <EM>first</EM> test')
match.group()

match=re.search(r'\d*\.?\d*','1432.75 +60.22i')
match.group()

match=re.search(r'\d*\.?\d*','1432 +60.22i')#? makes \.optional-stops after '2'
match.group()

match=re.search(r'[AGTC]+', 'the sequence ATTCGT')
match.group() 

match=re.search(r'\s+[A-Z]{1}\w+\s\w+', 'the bird shit frog`s name is Theolderma asper')

MyStr = 'Samraat Pawar, s.pawar@imperial.ac.uk, Systems biology and ecological theory'


match=re.search(r"[\w\s]*,\s[\w.@]*,\s[\w\s&]*", MyStr)
#add groups!!!
match=re.search(r"([\w\s*),\s([\w\.@]*),\s([\w\s&]*)", MyStr)

#then
match.group(1)
match.group(2)

