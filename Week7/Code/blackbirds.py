#!/usr/bin/env python
"""This script takes a file containing blackbird phylogenic data, performs some simple
text wrangling, then uses regex to find species, kingdom and phylum information. Finally
it outputs this data in a readable format"""
__author__ = 'Jack Rowntree jsr217@ic.ac.uk'
__version__ = '0.0.1'


import re


#read dataset
f = open('../Data/blackbirds.txt', 'r')
text = f.read()
f.close()

# remove \t\n and put a space in:
text = text.replace('\t',' ')
text = text.replace('\n',' ')


#~ text = text.decode('ascii', 'ignore')


# Using re.findall to output matches of a given regex, one for each variable 
Species=re.findall(r'Species\s[\w\s]+', text)
Kingdom=re.findall(r'Kingdom\s[\w]+', text)
Phylum=re.findall(r'Phylum\s[\w]+', text)


#Define a function that orders the results of regex
"""This is a simple function to arrange the regex results in a suitable output"""
def attacher(x,y,z):
	for i in range(4):
		print (x[i], ';', y[i] ,';' ,z[i])
#run function on the three results
attacher(Species, Kingdom, Phylum)

