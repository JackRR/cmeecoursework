#!/usr/bin/env python
"""This script is an example of how optimizing can reduce run time 
after using %run -p"""
__author__ = 'Jack Rowntree jsr217@ic.ac.uk'
__version__ = '0.0.'

"""assigns values of y sequentially from 1 to 100000000"""
def a_useless_function(x) :
	y = 0
	# eight zeros!
	for i in range(100000000) :
		y = y + i
	return 0	
	
"""assigns values of y sequentially from 1 to 100000"""
def a_less_useless_function(x) :
	y = 0
	# five zeros!
	for i in range(100000) :
		y = y + i
	return 0

"""runs the tw oprevious functions and prints the argument"""
def some_function(x) :
	print (x)
	a_useless_function(x)
	a_less_useless_function(x)
	return 0

some_function(1000)
