#!/usr/bin/env python
"""This simple script shows how subprocess can be used to 
run R scripts within the ipython shell"""
__author__ = 'Jack Rowntree jsr217@ic.ac.uk'
__version__ = '0.0.1'

import subprocess
subprocess.Popen("/usr.lib.R.bin.Rscript --verbose test.R > \
../Results/test.Rout 2> ../Results/test_errFile.Rout", \
shell=True).wait()
