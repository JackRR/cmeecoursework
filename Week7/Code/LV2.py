#!/usr/bin/env python
""" The typical Lotka-Volterra Model simulated using scipy. This script
takes command line arguments as parameters to the model. Then outputs the graph
of prey/predator population and saves in ../Results."""

__author__ = 'Jack Rowntree jsr217@ic.ac.uk'
__version__ = '0.0.1'

import sys
import scipy as sc 
import scipy.integrate as integrate
import pylab as p #Contains matplotlib for plotting

# import matplotlip.pylab as p #Some people might need to do this

def dR_dt(pops, t=0):
    """ Returns the growth rate of predator and prey populations at any 
    given time step """
    
    R = pops[0]
    C = pops[1]
    k=5 # carrying capacity of population
    p = (1-(R/k)) #prey density dependence
    dRdt = r*R*p - a*R*C 
    dydt = -z*C + e*a*R*C
    
    return sc.array([dRdt, dydt])

# Define parameters:
if len(sys.argv)>1:
	r = float(sys.argv[1]) # Resource growth rate= first argument in float type
	a = float(sys.argv[2]) # Consumer search rate= second argument in float type
	z = float(sys.argv[3])# Consumer mortality rate= third argument in float type
	e = float(sys.argv[4]) # Consumer production efficiency= fourth argument in float type
else:
	r = 1 # Resource growth rate= first argument in float type
	a = 0.1 # Consumer search rate= second argument in float type
	z = 0.5# Consumer mortality rate= third argument in float type
	e = 0.75 # Consumer production efficiency= fourth argument in float type

# Now define time -- integrate from 0 to 15, using 1000 points:
t = sc.linspace(0, 15,  1000)

x0 = 10
y0 = 5 
z0 = sc.array([x0, y0]) # initials conditions: 10 prey and 5 predators per unit area

pops, infodict = integrate.odeint(dR_dt, z0, t, full_output=True)

infodict['message']     # >>> 'Integration successful.'

prey, predators = pops.T 
f1 = p.figure() #Open empty figure object
p.plot(t, prey, 'g-', label='Resource density') # Plot
p.plot(t, predators  , 'b-', label='Consumer density')
p.grid()
p.legend(loc='best')
p.xlabel('Time')
p.ylabel('Population')
p.title('Consumer-Resource population dynamics')
tx='r = %s \na = %s \nz = %s \ne = %s' %(r, a, z , e)
p.text(10, 10, tx, fontsize=13,
        verticalalignment='top', bbox=dict(boxstyle='round', facecolor='white', pad=1, alpha =0.3))
p.show()
f1.savefig('../Results/prey_and_predators_2.pdf') #Save figure
