r'^abc[ab]+\s\t\d'
#find match starting with abc, followed by one or more 'a' or 'b' 
#followed by a space, followed by a tab ,followed by a numeric

r'^\d{1,2}\/\d{1,2}\/\d{4}$'
#find match starting with at least 1, at most 2 numeric characters 
#followed by '/' fllowed by 1-2 numerics, followed by '/', followed by
#4 numeric charcters at end of line

r'\s*[a-zA-Z,\s]+\s*'
#find match starting with 0 or more spaces ,followed by one or more 
#[upper/lowercase followed by space] followed by - or more spaces


r'[1-2]\d{1}[0-9]{2}[0-1][0-9][0-3]\d' #is {1} necessary?
