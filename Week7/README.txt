Week7 of CMEE 2017-2018. Python Week 2- networks, regex, profiling, Jupyter.
├── Code
│   ├── blackbirds.py:Using regex to extract useful data from a messy .txt
│   ├── DrawFW.py: script showing how networkx module works
│   ├── fmr.R: R script that plots lob metabolic rate vs body mass from the Nagy et al 1999 dataset
│   ├── LV1.py: Lotka Volterra model with default arguments
│   ├── LV2.py: Lotka Volterra model taking command line arguments
│   ├── MyFirstJupyterNb.ipynb: my first notebook!
│   ├── Nets.py: Script producing network in networkx of QMEE PhD collaborations
│   ├── Nets.R:Script producing network in igraph of QMEE PhD collaborations
│   ├── profileme.py: script showing how optimization can be reduced to reduced run time with %run -p
│   ├── re4.py: parsing web addresses with regex
│   ├── regexs.py: script showing the power of regex in python
│   ├── run_fmr_R.py: uses subprocess to run fmr.R
│   ├── run_LV.py:runs and profiles the two LV scripts
│   ├── testR.py:script showing how subprocess can be used to run a rudimentary R script
│   ├── testR.R:rudimentary R script to use in subprocess module
│   ├── timeitme.py:
│   └── using_os.py: script showing how the os module can be used to search home directory
├── Data
│   ├── blackbirds.txt: data used in blackbirds.py
│   ├── NagyEtAl1999.csv: data used in fmr.r and run_fmr_R.py
│   ├── QMEE_Net_Mat_edges.csv: data used in Nets.R/py
│   └── QMEE_Net_Mat_nodes.csv:data used in Nets.R/py
└── Results
