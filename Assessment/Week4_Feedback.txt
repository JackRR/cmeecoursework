Starting weekly assessment for Jack, Week4

Current Marks = 100

Note that: 
(1) Major sections begin with a double "====" line 
(2) Subsections begin with a single "====" line 
(3) Code output or text file content are printed within single "*****" lines 

======================================================================
======================================================================
Your Git repo size this week is about 509.38 MiB on disk 

PART 1: Checking project workflow...

Found the following directories in parent directory: Week6, Week1, Assessment, Week5, Week2, Week4, .git, Week3

Found the following files in parent directory: README.txt, .gitignore

Checking for key files in parent directory...

Found .gitignore in parent directory, great! 

Printing contents of .gitignore:
**********************************************************************
*∼ 
*.tmp
*.pyc
**********************************************************************

Found README in parent directory, named: README.txt

Printing contents of README.txt:
**********************************************************************
My CMEE 2017-2018 Coursework Repository.
This Repository contains CMEE coursework organised by week. For each week there is a Code,
Data and Sandbox directory, containing code, data used as input to code, and experimental 
files respectively. Week 1 Directory covers the work outlined in chapters 1-4 from the
SilBioComp.pdf, namely bash commands, shell scripting and latex. Week 2 covers chapters 5, 
which includes basic Python topics such as control statements, reading/writing csv,
debugging. Week 3 covers chapters 7, 8 and 9. This includes basic R topics such as those 
otutlined above for python, as well as data wrangling, vectorization, simulations and 
linear modelling. Week 4 overs the stats lectures given by Julia Schroeder, which includes
hypothesis testing, linear modelling, multiple linear modelling with interactions,
probability distributions.Week5 contains an example map-making python script from the QGIS 
practical. Involves raster merging, zonal stats and converting map file formats. 
	


**********************************************************************

======================================================================
Looking for the weekly directories...

Found 6 weekly directories: Week1, Week2, Week3, Week4, Week5, Week6

The Week4 directory will be assessed 

======================================================================
======================================================================
PART 2: Checking weekly code and workflow...

======================================================================
Assessing WEEK4...

Found the following directories: Code, Data, Results

Found the following files: Readme.txt

Checking for readme file in weekly directory...

Found README in parent directory, named: Readme.txt

Printing contents of Readme.txt:
**********************************************************************
Week 4 CMEE Coursework 2017/2018: StatswithSparrows Week

├── Code
│   ├── Rstats1_2.R: Covers R basics, probability distributions, basic plotting in base R
│   ├── Rstats15.R: Covers multiple linear modelling, interactions in linear models
│   ├── Rstats3_4_5: covers t test, hypothesis testing, standard errors
│   └── Stats6_7_8_9_10.R: covers basics of linear modelling using Tarsus Length data.
├── Data
│   └── SparrowSize.txt: data used across the Week 4 Notes

**********************************************************************

Found 4 code files: Rstats3_4_5.R, Stats6_7_8_9_10.R, Rstats1_2.R, Rstats15.R

Found the following extra files: Rplots.pdf
0.5 pt deducted per extra file

Current Marks = 99.5

======================================================================
Testing script/code files...

======================================================================
Inspecting script file Rstats3_4_5.R...

File contents are:
**********************************************************************
#!/usr/bin/env Rscript
#Author:Jack Rowntree
##StatsWithSparrows3############
#In this lesson we work out how to plot some common statistical ditributions, and look at the class types of the Tarsus data and how they are distributed#
require(ggplot2)
#reading the data file
d<-read.table("../Data/SparrowSize.txt", header=TRUE) 	
d1<-read.table("../Data/SparrowSize.txt", header=TRUE) 	
#plotting a histogram of the Year variable with suitable binwidth ; the data show an approimate normal distribution#
ggplot(data=d, aes(d$Year)) + 
  geom_histogram(col="red", fill="green", alpha=.2, binwidth=1)

#plotting a Gaussian distribution in ggplot2#
graphics.off()
xvalues<-data.frame(x=c(-3,3))
plot<-ggplot(xvalues, aes(x=xvalues))
plot +stat_function(fun=dnorm) +xlim(c(-4,4))+
  annotate("text",x=2.4, y=0.3, size=7, fontface="bold",
           label="Gaussian\nDistribution")
#plotting a poisson distribution in ggplot2#
dev.off()
ggplot(data.frame(x=c(0:100)), aes(x))+
  geom_point(aes(y=dpois(x,5)), colour="red")+
  xlim(c(0,50))+
    annotate("text",x=40, y=0.1, size=7, fontface="bold",
           label="Poisson\nDistribution")

##plotting a binomial distribution in ggplot2#
dev.off()
x1  <- seq(3,17)
df <- data.frame(x = x1, y = dbinom(x1, 20, 0.5))

plot1 <- ggplot(df, aes(x = x, y = y)) + geom_point(stat = "identity", col = "red", fill = "pink") + 
  scale_y_continuous(expand = c(0.01, 0)) + xlab("x") + ylab("Density") + 
  labs(title = "dbinom(x, 20, 0.5)") + theme_bw(16, "serif") + 
  theme(plot.title = element_text(size = rel(1.2), vjust = 1.5))
print(plot1)
#################################################################
#######StatswithSparrows4########################################
#In this lesson we learn about Standard errors- what they mean and how to calculate them#
#I calculated the SE for each variable below, first removing NA values from the vector#
clean<-d1$Tarsus[!is.na(d1$Tarsus)]
seTarsus<-sqrt(var(clean)/length(clean))
paste("Tarsus SE is", seTarsus)

clean<-d1$Mass[!is.na(d1$Mass)]
seMass<-sqrt(var(clean)/length(clean))
paste("Mass SE is", seMass)

clean<-d1$Bill[!is.na(d1$Bill)]
seBill<-sqrt(var(clean)/length(clean))
paste("Mass SE is", seBill)

clean<-d1$Wing[!is.na(d1$Wing)]
seTarsus<-sqrt(var(clean)/length(clean))
paste("Mass SE is", seMass)

###################################################################
###############StatsWithSparrows5##################################
#In this lesson we learn about the concepts of hypothesis testing, ad how to run t tests in R#

#Using a simple boxplot to get an idea of how mass is distributed differently in males and females
boxplot(d$Mass~d$Sex.1,  col  =  c("red",  "blue"),  ylab="Body  mass  (g)") 	

#looks like males are heavier, but use a t test to compare means and variance of two datasets#
t.test1  <- t.test(d$Mass~d$Sex.1) 	

t.test1 #Results show a mean difference of ~0.5g, with a very low p value The 95% CI tell us that 95% of time ,the difference between the sexes in mass will fall between ~0.4 and ~0.8. Obviously this is a weak effect- lets see how this is represetned in a truncated dataset#

shortd<-as.data.frame(head(d,  50)) 	

t.test2  <- t.test(shortd$Mass~shortd$Sex) 	

t.test2 	
#With a lower sample size, our effect is no longer statistically significant- Large N required to show small effect sizes!

**********************************************************************

Testing Rstats3_4_5.R...

Output (only first 500 characters): 

**********************************************************************
null device 
          1 
null device 
          1 
[1] "Tarsus SE is 0.0209621095716943"
[1] "Mass SE is 0.0513208953507819"
[1] "Mass SE is 0.0177121726986748"
[1] "Mass SE is 0.0513208953507819"

	Welch Two Sample t-test

data:  d$Mass by d$Sex.1
t = -5.5654, df = 1682.9, p-value = 3.039e-08
alternative hypothesis: true difference in means is not equal to 0
95 percent confidence interval:
 -0.7669117 -0.3672162
sample estimates:
mean in group female   mean in group male 
            27.46852  
**********************************************************************

Encountered error:
Loading required package: ggplot2
Warning message:
Removed 50 rows containing missing values (geom_point). 

======================================================================
Inspecting script file Stats6_7_8_9_10.R...

File contents are:
**********************************************************************
#!/usr/bin/env Rscript
#Author:Jack Rowntree
#########################
## StatswithSparrows6 ##
#########################

#install.packages("pwr")
library("pwr")

pwr.t.test(d=(0-5)/1-(0.8*0.05), power = 0.8, sig.level = 0.05, type = "two.sample", alternative = "two.sided")
# Sample 6 to reduce chance of Type II error (we got 2.1 -> round it up --> multiply by two as we are sampling males and females (two different groups/categories)). The effect size (d) is 5.04 

####################### LECTURE 7 ##########################
#How do df's affect Student's t? As df increase, the t-distribution approachs normality - the center increases, while the area near the tail decreases. The more df, the larger the sample size and the t-distribution is more peaked (less spread out), so a smaller difference between means and mean (or criterion value) is required for us to say that there is actually a difference.

#Why are the df's in a dataset of 100 birds on a 2-sample t-test 98? In a two sample t-test with 100 samples, we can deduce each of the two samples with n-1 (we can work out the last one via subtraction when we know n-1 pbservations lus the total number of observations) - because there are 2 samples we have to subtract 2, 1 for each sample.


####################### LECTURE 9 ##########################
# Fitting models to data
# Linear models
# No handout for lecture.

x <- c(1,2,3,4,8)
y <- c(4,3,5,7,9)

model1 <- (lm(y~x)) #generating a linear model.
model1

summary(model1) #here are various functions that R has to provide more information about a linear model
anova(model1)
resid(model1)
cov(x,y)
var(x)
plot(y~x)

####################### HANDOUT 10 ###########################
rm(list=ls())
d<-read.table("../Data/SparrowSize.txt", header=T)

plot(d$Mass~d$Tarsus, ylab="Mass (g)", xlab="Tarsus (mm)", pch=19, cex=0.4)

#Making a line using the formular y = mx + b
x<-c(1:100)
b<-0.5
m<-1.5
y<- m*x + b
plot(x,y, xlim=c(0,100), ylim=c(0,100), pch=19, cex=0.5)

#Exploring the indices of our data.
d$Mass[1]
length(d$Mass)
d$Mass[1770]

plot(d$Mass~d$Tarsus, ylab="Mass (g)", xlab="Tarsus (mm)", pch=19, cex=0.4, ylim=c(-5,38), xlim=c(0,22))
plot(d$Mass~d$Tarsus, ylab="Mass (g)", xlab="Tarsus (mm)", pch=19, cex=0.4) #Using original plot rather than one where we define limits, where it is harder to see the data.

#Creating a subset of the data to strip missing values.
d1 <- subset(d, d$Mass!="NA")
d2 <- subset(d1, d1$Tarsus!="NA")
length(d2$Tarsus)

model1 <- lm(Mass~Tarsus, data=d2) #Creating linear model.
summary(model1)

hist(model1$residuals) #Looking at distributio of residuals.
head(model1$residuals)


model2 <- lm(y~x) #Using x and y we generated before.
summary(model2)

d2$z.Tarsus <- scale(d2$Tarsus) #Adding a new column to d2.
model3 <- lm(Mass~z.Tarsus, data=d2) #Making a new linear model. Running it with z scores.

plot(d2$Mass~d2$z.Tarsus, pch=19, cex=0.4)
abline(v = 0, lty = "dotted")

#Plotting graphs
d$Sex <- as.numeric(d$Sex)
par(mfrow = c(1, 2))
plot(d$Wing ~ d$Sex.1, ylab="Wing (mm)")
plot(d$Wing ~ d$Sex, xlab="Sex",xlim=c(-0.1,1.1), ylab="")
abline(lm(d$Wing ~ d$Sex, lwd=2))
text(0.15, 76, "intercept")
text(0.9, 77.5, "slope", col = "red")

d4 <- subset(d, d$Wing!="NA")
m4 <- lm(Wing~Sex, data=d4)
t4 <- t.test(d4$Wing~d4$SEx, var.equal=TRUE) #Carrying out a t-test.
summary(m4) #Looking at results of t-test.

par(mfrow=c(2,2))
plot(model3)**********************************************************************

Testing Stats6_7_8_9_10.R...

Output (only first 500 characters): 

**********************************************************************

**********************************************************************

Encountered error:
Error in library("pwr") : there is no package called ‘pwr’
Execution halted

======================================================================
Inspecting script file Rstats1_2.R...

File contents are:
**********************************************************************
#!/usr/bin/env Rscript
#Author:Jack Rowntree
#Notes from Lessons 1 and 2 of Stats Week


## StatswithSparrows1 ##

#R has basic mathematical functions:
2*2+1
2*(2+1)
12/2^3
(12/2)^3

#You can assign values to variables:
x <- 5
x2 <- x^2
a <- x2 + x
z <- sqrt((x * x2)/1.24)

#There are logical operators:
3 > 2
3 >= 3
4 < 2

#You can create vectors of different formats:
myNumericVector <- c(1.3,2.5,1.9,3.4,5.6,1.4,3.1,2.9) 	
myCharacterVector <- c("low","low","low","low","high","high","high","high") 	
myLogicalVector <- c(TRUE,TRUE,FALSE,FALSE,TRUE,TRUE,FALSE,FALSE) 
myMixedVector <- c(1, TRUE, FALSE, 3, "help", 1.2, TRUE, "notwhatIplanned")	

str(myNumericVector) #To see structure of any variable use str()
str(myCharacterVector)
str(myLogicalVector)
str(myMixedVector)

#Installing and loading packages:
#install.packages("lme4")
library(lme4) #Loads package with warning.
require(lme4) #Loads package without warning.

#We can ask R for help using help() function:

#Entering external data:
d <- read.table("../Data/SparrowSize.txt", header=TRUE) #Load a data frame.
str(d) #Look at structure of dataframe.

## StatswithSparrows2 ##

d<-read.table("../Data/SparrowSize.txt", header = TRUE) #read in .txt file 
str(d)
head(d)
length(d$Tarsus)
hist(d$Tarsus)
ggplot(data=d, aes(d$Tarsus)) + #plotting a histogram in ggplot!!!
  geom_histogram(col="red", fill="green", alpha=.2, breaks =seq(15, 22, by=.5))
mean(d$Tarsus, na.rm=TRUE) #na.rm removes NAs, calculates mean
median(d$Tarsus, na.rm=TRUE) #na.rm removes NAs, calculates median

##calculating variance without using the very heplful var() fuction; using modeest::mlv() to estimate mode of sample

require(modeest)
# mlv(d$Tarsus)
# d1<-d[!is.na(d$Tarsus),]
# mlv(d1$Tarsus)
# mu<-mean(d1$Tarsus)
# sum((d1$Tarsus-mu)^2)/(length(d1$Tarsus-1))
# sum((d1$Tarsus - mean(d1$Tarsus))^2)/(length(d1$Tarsus) - 1) 	
# 
# #Calculating Z scores
# zTarsus <- (d2$Tarsus - mean(d2$Tarsus))/sd(d2$Tarsus)
# var(zTarsus)
# sd(zTarsus)
# hist(zTarsus)

#install.packages("mosaic")
require(mosaic) #This package calculates Z-scores.

set.seed(123) #Setting seed so we get the same 'random' numbers.
znormal <- rnorm(1e+06) #random generation of normal distribution 
hist(znormal, breaks = 100)
summary(znormal) #gives a  summary of the normal data, including quantiles, min and centrral tendencies. The last quantiles are important when we do hypothesis testing.

#Generating graphs that display the 5%, 95% quantiles and median
par(mfrow = c(1, 2)) 	
hist(znormal, breaks = 100) 	
abline(v = qnorm(c(0.25, 0.5, 0.75)), lwd = 2) 	
abline(v = qnorm(c(0.025, 0.975)), lwd = 2, lty = "dashed") 	
plot(density(znormal)) 	
abline(v = qnorm(c(0.25, 0.5, 0.75)), col = "gray") 	
abline(v = qnorm(c(0.025, 0.975)), lty = "dotted", col = "black") 	
abline(h = 0, lwd = 3, col = "blue") 	
text(2, 0.3, "1.96", col = "red", adj = 0) 	
text(-2, 0.3, "-1.96", col = "red", adj = 1)
#95% CI is v important - it is the range of values that encompass the population true value with 95% probability.

**********************************************************************

Testing Rstats1_2.R...

Output (only first 500 characters): 

**********************************************************************
[1] 5
[1] 6
[1] 1.5
[1] 216
[1] TRUE
[1] TRUE
[1] FALSE
 num [1:8] 1.3 2.5 1.9 3.4 5.6 1.4 3.1 2.9
 chr [1:8] "low" "low" "low" "low" "high" "high" "high" ...
 logi [1:8] TRUE TRUE FALSE FALSE TRUE TRUE ...
 chr [1:8] "1" "TRUE" "FALSE" "3" "help" "1.2" "TRUE" ...
'data.frame':	1770 obs. of  8 variables:
 $ BirdID: int  1 2 2 2 2 2 2 2 2 2 ...
 $ Year  : int  2002 2001 2002 2003 2004 2004 2004 2004 2004 2005 ...
 $ Tarsus: num  16.9 16.8 17.2 17.5 17.8 ...
 $ Bill  : num  NA NA NA 13.5 13.4 ...
 
**********************************************************************

Encountered error:
Loading required package: Matrix
Loading required package: methods
Error: could not find function "ggplot"
Execution halted

======================================================================
Inspecting script file Rstats15.R...

File contents are:
**********************************************************************
#!/usr/bin/env Rscript
#Author:Jack Rowntree
###################StatsWithSparrows15##################
###In this lesson we learn more about linear models- multiple linear models, interaction effects and how to interpret these results####

#reading the data file
 d<-read.table("../Data/SparrowSize.txt", header=TRUE) 	
 
#linear model of the effect of Tarsus Length and Sex on Mass
sexmodel<-lm(formula= Mass ~ Tarsus + Sex, data=d)
summary

#linear model of the effect of Tarsus Length and Sex on Mass with interaction of Sex and Tarsus
sexinteraction<-lm(formula= Mass ~ Tarsus * Sex, data=d)
summary(sexinteraction)

#linear model of the effect of Tarsus Length and Sex on Bill Length
sexmodel<-lm(formula= Bill ~ Tarsus + Sex, data=d)
summary(sexmodel)
plot(sexmodel)

**********************************************************************

Testing Rstats15.R...

Output (only first 500 characters): 

**********************************************************************
function (object, ...) 
UseMethod("summary")
<bytecode: 0x2f23370>
<environment: namespace:base>

Call:
lm(formula = Mass ~ Tarsus * Sex, data = d)

Residuals:
    Min      1Q  Median      3Q     Max 
-8.0137 -1.2489 -0.1464  1.1437  7.6765 

Coefficients:
            Estimate Std. Error t value Pr(>|t|)    
(Intercept)   5.0407     1.3887   3.630 0.000292 ***
Tarsus        1.2167     0.0752  16.180  < 2e-16 ***
Sex           2.3247     1.9609   1.186 0.235972    
Tarsus:Sex   -0.1041     0.1057 
**********************************************************************

Code ran without errors

Time consumed = 0.16821s

======================================================================
======================================================================
Finished running scripts

Ran into 3 errors

======================================================================
======================================================================

FINISHED WEEKLY ASSESSMENT

Current Marks for the Week = 99.5

NOTE THAT THESE ARE NOT THE FINAL MARKS FOR THE WEEK, BUT AN UPPER BOUND ON THE MARKS!