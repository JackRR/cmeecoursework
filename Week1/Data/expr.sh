echo "Creating a space delimited version of $1 ..."
cat $1 | tr -s "," " " > $1.txt

ext="csv.txt"
ext2="txt"
for i in $(ls *.$ext)
do
  mv -f $i ${i%.$ext}.$ext2
done
