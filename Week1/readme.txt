└── Week1 of CMEE Coursework 2017/2018: covers chapters 1-4 from the
    SilBioComp.pdf (bash commands, shell scripting and latex)

    ├── Code
    │   ├── boilerplate.sh: simple boilerplate for shell scripts
    │   ├── compilelatex.sh: compile .tex file and output pdf  with bibliography, open in  evince
    │   ├── ConcatenateTwoFiles.sh: concatenates the contents of two files
    │   ├── CountLines.sh: counts number of lines in file
    │   ├── csvtospace.sh: substitute the commas in the file with spaces
    │   ├── firstbiblio.bib: bibtex file for FirstExample.tex
    │   ├── FirstExample.tex: Latex code for simplified scientific article
    │   ├── MyExampleScript.sh: A simple script showing how variable assignment works
    │   ├── tabtocsv.sh: substitute the tabs in the files with commas
    │   ├── UniXPrac1: Bash scripts on fasta data
    │   └── variables.sh: Another simple script showing how variable assignment works 
    ├── Data
    │   ├── 1800.csv: input to csvtospace.sh (Temperature data)
    │       ├── 1800.csv.txt: output from csvtospace.sh
	|├── 1801.csv: file input to csvtospace.sh (Temperature data)
    │       ├── 1801.csv.txt: input from csvtospace.sh
        ├── 1802.csv: input tocsvtospace.sh (Temperature data)
    │       ├── 1802.csv.txt: output from csvtospace.sh
        ├── 1803.csv: input to csvtospace.sh (Temperature data)
    │       ├── 1803.csv.txt: output from csvtospace.sh
    │   ├── 407228326.fasta: data used for UnicPrac1.txt (genomic data)
    │   ├── 407228412.fasta: data used for UnicPrac1.txt  (genomic data)
    │   └── E.coli.fasta: data used for UnicPrac1.txt  (genomic data)