#!/bin/bash
# Author: Jack Rowntree JSR217@ic.ac.uk
# Script: variables.sh
# Desc: simple script showing how variables are assigned
# Arguments: none
# Date: Oct 2017

#!/bin/bash
MyVar='some string'
echo 'the current value of the variable is' $MyVar
echo 'Please enter a new string'
read MyVar
echo 'the current value of the variable is' $MyVar
## Reading multiple values
echo 'Enter two numbers separated by space(s)'
read a b
echo 'you entered' $a 'and' $b'. Their sum is:'
mysum=`expr $a + $b`
echo $mysum
