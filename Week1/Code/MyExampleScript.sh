#!/bin/bash
# Author: Jack Rowntree JSR217@ic.ac.uk
# Script: MyExampleScript.sh
# Desc: simple script showing how variables are assigned
# Arguments: none
# Date: Oct 2017

#!/bin/bash
msg1="Hello"
msg2=$USER
echo "$msg1 $msg2"
echo "Hello $USER"
echo
