#!/bin/bash
# Author: Jack Rowntree JSR217@ic.ac.uk
# Script: tabtocsv.sh
# Desc: substitute the commas in the file with spaces
# saves the output into a new txt file
# Arguments: 1-> comma seperated file
# Date: Oct 2017




echo "Creating a space delimited version of $1 ..."
cat $1 | tr -s "," " " > $1.txt


echo "Finito!"

