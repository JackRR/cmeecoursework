#!/bin/bash
# Author: Jack Rowntree JSR217@ic.ac.uk
# Script: CountLines.sh
# Desc: counts number of lines in file
# Arguments: 1-> any file
# Date: Oct 2017

#!/bin/bash
NumLines=`wc -l < $1`
echo "The file $1 has $NumLines lines"
echo
