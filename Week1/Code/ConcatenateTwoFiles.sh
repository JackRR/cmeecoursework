#!/bin/bash
# Author: Jack Rowntree JSR217@ic.ac.uk
# Script: ConcatenateTwoFiles.sh
# Desc: concatenates the contents of two files
# Arguments: 3:2 files to be concatenated + output file
# Date: Oct 2017

#!/bin/bash
cat $1 > $3
cat $2 >> $3
echo "Merged File is"
cat $3
