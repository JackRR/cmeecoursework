└── Week2 of CMEE Coursework 2017/2018: covers chapter 5 from the
    SilBioComp.pdf (basic python)


├── Code
│   ├── align_seqs_fasta.py: Aligns any two fasta files, writes a csv with the best match.
│   ├── align_seqs.py: matches two default DNA strings, writes a csv with best match
│   ├── basic_csv.py:: Script that shows how to read and write with the csv module using for loops.
│   ├── basic_io.py: Script that shows how to read and write .txt files
│   ├── boilerplate.py: Simple boilerplate script demonstrating the components of python functions 
│   ├── cfexercises.py: Some functions exemplifying the use of if statemsnts
│   ├── control_flow.py: Some functions exemplifying the use of control statements
│   ├── debugme.py: Small script with which to run pdb
│   ├── dictionary.py: This scipt creates a dictionary of sets grouped by order from the taxa data
│   ├── lc1.py: This script finds the latin/common names/body masses from birds data, as LCs or for loops
│   ├── lc2.py: This scipt creates a dictionary of sets grouped by order from the taxa data
│   ├── loops.py: Some functions exemplifying the use of loops in python
│   ├── oaks.py: Reads a small list of tree species names, adds them into a set if in quercus genus.
│   ├── scope.py: Simple script demonstrating how scoping works in python
│   ├── sysargv.py: Simple script showing how sys.argv works in python
│   ├── test_control_flow.py: Some functions exemplifying the use of doctesting and control statements
│   ├── test_oaks.py: If an entry in the dataset is i the genus Quercus, writes to csv.
│   ├── tuple.py; Prints list of tuples with each tuple on seperate line, with each component of tuple labelled"""
│   └── using_name.py: Simple script explaining the thniking behind the "if ___name___..." convention
├── Data
│   ├── 407228326.fasta: Fasta file used for align_seqs_fasta.py
│   ├── 407228412.fasta: Fasta file used for align_seqs_fasta.py
│   ├── align.csv: DNA sequence used for align_seqs.py
│   └── TestOaksData.csv: data used for test_oaks.py
