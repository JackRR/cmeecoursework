#!/usr/bin/python
"""Simple script showing how sys.argv works in python"""
__author__ = 'Jack Rowntree SR217@ic.ac.uk'
__version__ = '0.0.1'

import sys
print "This is the name of the script: ", sys.argv[0]
print "Number of arguments: ", len(sys.argv)
print "The arguments are: ", str(sys.argv)
