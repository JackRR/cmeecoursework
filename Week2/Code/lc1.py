#!/usr/bin/env python
"""This script finds the latin, common names and body masses from the birds data, as list comprehensions or simple for loops"""
__author__ = 'Jack Rowntree jsr217@ic.ac.uk'
__version__ = '0.0.1'

birds = ( ('Passerculus sandwichensis','Savannah sparrow',18.7),
          ('Delichon urbica','House martin',19),
          ('Junco phaeonotus','Yellow-eyed junco',19.5),
          ('Junco hyemalis','Dark-eyed junco',19.6),
          ('Tachycineata bicolor','Tree swallow',20.2),
         )
         
latin = []
common = []
masses = []

"""prints all latin, common names and body masses using for loop"""
for x in birds:
     latin.append(x[0])
     common.append(x[1])
     masses.append(x[2])	
print(latin)
print(common)
print(masses)

	
################
"""create list comprehensions for latin/common names and body mass and prints"""
latin_lc=[x[0] for x in birds]
common_lc=[x[1] for x in birds]
masses_lc=[x[2] for x in birds]
print latin_lc
print common_lc
print masses_lc




