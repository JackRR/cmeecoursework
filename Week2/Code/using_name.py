#!/usr/bin/python
# Filename: using_name.py62
"""Simple script explaining the thniking behind the if ___name___ convention"""
__author__ = 'Jack Rowntree SR217@ic.ac.uk'
__version__ = '0.0.1'

if __name__ == '__main__':
	print 'This program is being run by itself'
else:
	print 'I am being imported from another module'
