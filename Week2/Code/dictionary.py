#!/usr/bin/env python
"""This scipt creates a dictionary of sets grouped by order from the taxa data"""
__author__ = 'Jack Rowntree jsr217@ic.ac.uk'
__version__ = '0.0.1'

taxa = [ ('Myotis lucifugus','Chiroptera'),
         ('Gerbillus henleyi','Rodentia',),
         ('Peromyscus crinitus', 'Rodentia'),
         ('Mus domesticus', 'Rodentia'),
         ('Cleithrionomys rutilus', 'Rodentia'),
         ('Microgale dobsoni', 'Afrosoricida'),
         ('Microgale talazaci', 'Afrosoricida'),
         ('Lyacon pictus', 'Carnivora'),
         ('Arctocephalus gazella', 'Carnivora'),
         ('Canis lupus', 'Carnivora'),
        ]
#initialise an empty dictionary        
my_dict=dict()

for row in taxa:
	if row[1] not in my_dict:  #if order name not in a dictionary key
		my_dict[row[1]] = set() #add a key of order name with empty set value
	my_dict[row[1]].add(row[0]) #index the dicionary by this row's order name and add species name values
print my_dict #print :)
	
	
	 
   


