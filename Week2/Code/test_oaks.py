#!/usr/bin/env python
"""This script reads the oaks data csv file, determines if the entry is a
member of the quercus genus, and write those that fulfil this criterion
into a new .csv file."""
__author__ = 'Jack Rowntree jsr217@ic.ac.uk'
__version__ = '0.0.1'

import csv
import sys
import pdb
import doctest

#Define function
def is_an_oak(name):
    """ Returns True if input string is'quercus'

       >>> is_an_oak('quercus')
       True
       
       >>> is_an_oak('Fagus')
       False
      
    """
###If the input is exactly "quercus" i.e. row[0] of TestOaksData.csv, returns TRUE
    return name.lower()== 'quercus'

print(is_an_oak.__doc__)

"""runs is_an_oak on the oaks data set, write TRUE entries to a new csv file. It also prints into the terminal both oak and non-oak data"""
def main(argv): 
    f = open('../Data/TestOaksData.csv','rb')
    g = open('../Results/JustOaksData.csv','wb')
    taxa = csv.reader(f)
    csvwrite = csv.writer(g)
    oaks = set()
    for row in taxa: ##iterating over rows, print info; if is_an_oak()=TRUE, do x and write to csv, if false, do y
        print row
        print "The genus is", row[0]
        if is_an_oak(row[0]):
            #~ print row[0]
            print 'FOUND AN OAK!'
            print " "
            csvwrite.writerow([row[0], row[1]])
	else:
            print 'THAT AINT AN OAK G' 
               
    return 0  

if (__name__ == "__main__"):
    status = main(sys.argv)
doctest.testmod()


 
