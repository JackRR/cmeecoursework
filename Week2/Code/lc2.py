#!/usr/bin/env python
"""This scipt creates a dictionary of sets grouped by order from the taxa data"""
__author__ = 'Jack Rowntree jsr217@ic.ac.uk'
__version__ = '0.0.1'


# Average UK Rainfall (mm) for 1910 by month
# http://www.metoffice.gov.uk/climate/uk/datasets
rainfall = (('JAN',111.4),
            ('FEB',126.1),
            ('MAR', 49.9),
            ('APR', 95.3),
            ('MAY', 71.8),
            ('JUN', 70.2),
            ('JUL', 97.1),
            ('AUG',140.2),
            ('SEP', 27.0),
            ('OCT', 89.4),
            ('NOV',128.4),
            ('DEC',142.2),
           )
           
# (1) Use a list comprehension to create a list of month/temperature tuples
# where the amount of rain was less than 50 mm.            
rain_100_lc= [tuple for tuple in rainfall if tuple[1] > 100]
print rain_100_lc
 
# (2) Use a list comprehension to create a list of just month names where the
# amount of rain was less than 50 mm. 
rain_50_lc= [tuple[0] for tuple in rainfall if tuple[1] <50]
print rain_50_lc

# (3) Now do (1) and (2) using conventional loops 
rain_100 = []
for month in rainfall:
	if month[1]>100:
		rain_100.append(month)
print rain_100

rain_50 = []
for month in rainfall:
	if month[1]<50:
		rain_50.append(month[0])
print rain_50
