#!/usr/bin/python
"""Script that shows how to read and write with the csv module. For loops are used
to iterate the output of tuples from first csv, and the writing 
each row of the csv file in the second case """
__author__ = 'Jack Rowntree SR217@ic.ac.uk'
__version__ = '0.0.1'


import csv
# Read a file containing:
# 'Species','Infraorder','Family','Distribution','Body mass male (Kg)'
f = open('../Sandbox/testcsv.csv','rb')
csvread = csv.reader(f)
temp = []
for row in csvread:
	temp.append(tuple(row))
	print row
	print "The species is", row[0]
f.close()

# write a file containing only species name and Body mass
f = open('../Sandbox/testcsv.csv','rb')
g = open('../Sandbox/bodymass.csv','wb')
csvread = csv.reader(f)
csvwrite = csv.writer(g)
for row in csvread:
	print row
	csvwrite.writerow([row[0], row[4]])
f.close()
g.close()
