#!/usr/bin/python
"""Simple script demonstrating how scoping works in python"""
__author__ = 'Jack Rowntree SR217@ic.ac.uk'
__version__ = '0.0.1'

##First 
_a_global = 10
"""prints values of two variables defined within function"""
def a_function():
		_a_global = 5
		_a_local = 4
		print "Inside the function, the value is ", _a_global
		print "Inside the function, the value is ", _a_local
		return None
"""prints a_global, which retains the value described in the functions parent environmnet"""		
a_function()
print "Outside the function, the value is ", _a_global

##Then

_a_global = 10
"""prints values of two variables defined within function"""
def a_function():
		global _a_global
		_a_global = 5
		_a_local = 4
		print "Inside the function, the value is ", _a_global
		print "Inside the function, the value is ", _a_local
"""prints a_global, which retains the value described within the module as the global command was used"""
		
a_function()
print "Outside the function, the value is", _a_global

