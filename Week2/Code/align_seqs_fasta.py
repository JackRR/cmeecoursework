#!/usr/bin/env python
"""This script takes as an input any two fasta files; if no arguments are supplied
,the defaults are the Week1 fasta files. It determines the longer sequence,
 and loops through the two files base by base t ofind matches
 Then loops through each startpoint of the longer sequence and repeats this.
 The best match is found and printed to the terminal with the two sequences
 in the best matched position"""
__author__ = 'Jack Rowntree jsr217@ic.ac.uk'
__version__ = '0.0.1'

import csv
import sys
import re

"""finds number of matches between wto sequences at a given startpoint"""
def calculate_score(s1, s2, l1, l2, startpoint):
    # startpoint is the point at which we want to start
    matched = "" # contains string for alignement
    score = 0
    for i in range(l2):
        if (i + startpoint) < l1:
            # if its matching the character
            if s1[i + startpoint] == s2[i]:
                matched = matched + "*"
                score = score + 1
            else:
                matched = matched + "-"
#The following lines print the 2 sequences and score/matching info-
#I removed them so they are not iterated in the next for loop                  
    # build some formatted output
    #~ print "." * startpoint + matched           
    #~ print "." * startpoint + s2
    #~ print s1
    #~ print score 
    #~ print ""

    return score


"""Reads the two files, and removes unwanted characters, finds longer sequence, runs
calculate_score() for each startpoint, prints best match with scores and writes this
to a new .csv"""
def main(argv):
	if len(sys.argv) == 1:
		f=open("../Data/407228326.fasta", 'rb')
		g=open("../Data/407228412.fasta", 'rb')
	elif len(sys.argv) == 2:
		print("Please enter two fasta files to match")
		sys.exit(0)
	else:
		f = open(argv[1], 'rb')
		g = open(argv[2], 'rb')
		
	read = f.read()
	seqq=re.sub('\>.*', '', read)
	seq1= re.sub(r'\n', '',seqq)
	
	read1 = g.read()
	seqq2=re.sub('\>.*', '', read1)
	seq2= re.sub(r'\n', '',seqq2)
	# assign the longest sequence s1, and the shortest to s2
	# l1 is the length of the longest, l2 that of the shortest

	l1 = len(seq1)
	l2 = len(seq2)
	if l1 >= l2:
		s1 = seq1
		s2 = seq2
	else:
		s1 = seq2
		s2 = seq1
		l1, l2 = l2, l1 # swap the two lengths

	# function that computes a score
	# by returning the number of matches 
	# starting from arbitrary startpoint

	calculate_score(s1, s2, l1, l2, 0)
	calculate_score(s1, s2, l1, l2, 1)
	calculate_score(s1, s2, l1, l2, 5)

	# now try to find the best match (highest score)
	my_best_align = None
	my_best_score = -1

	for i in range(l1): #for each startpoint, calculate score
		z = calculate_score(s1, s2, l1, l2, i)
		if z > my_best_score:#if score is better than best, assign value to my_best_score
			my_best_align = "." * i + s2
			my_best_score = z

	print my_best_align
	print s1
	score= "Best score:", my_best_score
	print "Best score:", my_best_score

	best = csv.writer(open('../Results/aligned_sequences_fasta.csv', 'wb'), delimiter=',')
	x= my_best_align, s1, score
	best.writerows(x)
	
	
if(__name__ == "__main__"):
	status = main(sys.argv)
	sys.exit(status)
