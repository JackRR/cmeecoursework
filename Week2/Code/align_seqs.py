__author__ = 'Jack Rowntree (jsr217@ic.ac.uk)'
__version__ = '0.0.1'
"""This script runs alignment analysis on two specified fasta files. It determines the longer sequence,
 and loops through the two files base by base to find matches
 Then loops through each startpoint of the longer sequence and repeats this.
 The best match is found and printed to the terminal with the two sequences
 in the best matched position"""  
# imports
import sys # module to interface our program with the operating system
import csv
import linecache

# function that computes a score
# by returning the number of matches 
# starting from arbitrary startpoint
def calculate_score(s1, s2, l1, l2, startpoint):
    # startpoint is the point at which we want to start
    matched = "" # contains string for alignement
    score = 0
    for i in range(l2):
        if (i + startpoint) < l1:
            # if its matching the character
            if s1[i + startpoint] == s2[i]:
                matched = matched + "*"
                score = score + 1
            else:
                matched = matched + "-"

    # build some formatted output
    print "." * startpoint + matched           
    print "." * startpoint + s2
    print s1
    print score 
    print ""

    return score
    
    
def main(argv) :
	f = open('../Data/align.csv','rb')
	g = open('../Results/aligned_sequences.csv','wb')
	seq = csv.reader(f)
	csvwrite = csv.writer(g)
	for row in seq :
		seq1 = row[0]
		seq2 = row[1]


# assign the longest sequence s1, and the shortest to s2
# l1 is the length of the longest, l2 that of the shortest

	l1 = len(seq1)
	l2 = len(seq2)
	if l1 >= l2:
		s1 = seq1
		s2 = seq2
	else:
		s1 = seq2
		s2 = seq1
		l1, l2 = l2, l1 # swap the two lengths



	calculate_score(s1, s2, l1, l2, 0)
	calculate_score(s1, s2, l1, l2, 1)
	calculate_score(s1, s2, l1, l2, 5)

	# now try to find the best match (highest score)
	my_best_align = None
	my_best_score = -1

	for i in range(l1): #if score is better than best, assign value to my_best_score
		z = calculate_score(s1, s2, l1, l2, i)
		if z > my_best_score:
			my_best_align = "." * i + s2
			my_best_score = z

	print my_best_align
	print s1
	print "Best score:", my_best_score

	g.write("For the sequences: " + str(my_best_align) + " & " + str(s1) + " the best score is " + str(my_best_score))

#~ def main(argv) :
	#~ # sys.exit("don't want to do this right now!")
	#~ return 0
		
if (__name__ == "__main__"): #makes sure the "main" function is called from commandline
		status = main(sys.argv)
		sys.exit(status)
		
