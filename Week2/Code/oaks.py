#!/usr/bin/env python
"""This script reads a small list of tree species names, 
and adds them into a set if they are of the quercus genus.
List comprehension and for loop methods are used, also
using upper case conversion"""

__author__ = 'Jack Rowntree jsr217@ic.ac.uk'
__version__ = '0.0.1'


taxa = [ 'Quercus robur',
'Fraxinus excelsior',
'Pinus sylvestris',
'Quercus cerris',
'Quercus petraea',
]

def is_an_oak(name):
	return name.lower().startswith('quercus')
	
	
##Using for loops
oaks_loops = set()
for species in taxa:
		if is_an_oak(species):
			oaks_loops.add(species)
print oaks_loops

##Using list comprehensions
##Using list comprehensions
oaks_lc = set([species for species in taxa if is_an_oak(species)])
print oaks_lc

##Get names in UC with for loop
oaks_loops = set()
for species in taxa:
	if is_an_oak(species):
		oaks_loops.add(species.upper())
print oaks_loops

##Get names in UC with list compreheensions
oaks_lc = set([species.upper() for species in taxa if is_an_oak(species)])
print oaks_lc
