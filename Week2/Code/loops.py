#!/usr/bin/env python
"""Some functions exemplifying the use of loops in python"""
__author__ = 'Jack Rowntree jsr217@ic.ac.uk'
__version__ = '0.0.1'

#For loops in PYthon
for i in range(5):
	print i
	
my_list= [0,2, "geronimo!", 3.0, True, False]
for k in my_list:
	print k
	
total = 0
summands = [0,1,11,111,1111]
for s in summands:
	total = total + s
	print summands
#while loops in Python
z=0
while z <100:
	z = z + 1
	print(z)
	
b= True
while b:
		print "Geronimo! ininity!!!!!!"
