#!/usr/bin/env python
"""Prints list of tuples with each tuple on seperate line, with each component of tuple labelled"""
__author__ = 'Jack Rowntree jsr217@ic.ac.uk'
__version__ = '0.0.1'
birds = ( ('Passerculus sandwichensis','Savannah sparrow',18.7),
          ('Delichon urbica','House martin',19),
          ('Junco phaeonotus','Yellow-eyed junco',19.5),
          ('Junco hyemalis','Dark-eyed junco',19.6),
          ('Tachycineata bicolor','Tree swallow',20.2),
        )

#iterates over row, taking each elemnt of the tuple and appending text strings
for x, y, z in birds:
	print "Latin name: " + x + "; " + "Species name: " + y +  "; " + "Body Mass: "  +str(z)
