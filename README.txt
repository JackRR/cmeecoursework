My CMEE 2017-2018 Coursework Repository.
This Repository contains CMEE coursework organised by week. For each week there is a Code,
Data and Sandbox directory, containing code, data used as input to code, and experimental 
files respectively. Week 1 Directory covers the work outlined in chapters 1-4 from the
SilBioComp.pdf, namely bash commands, shell scripting and latex. Week 2 covers chapters 5, 
which includes basic Python topics such as control statements, reading/writing csv,
debugging. Week 3 covers chapters 7, 8 and 9. This includes basic R topics such as those 
otutlined above for python, as well as data wrangling, vectorization, simulations and 
linear modelling. Week 4 overs the stats lectures given by Julia Schroeder, which includes
hypothesis testing, linear modelling, multiple linear modelling with interactions,
probability distributions.Week5 contains an example map-making python script from the QGIS 
practical. Involves raster merging, zonal stats and converting map file formats. Week 6
covers Jason Hodgson's week on genomic analysis in R and bash. Week 7 returns to python, 
covering more advanced python topics such as regex, networks, profiling and Jupyter.
	


