Week 3 CMEE Coursework 2017/2018: covers chapter 7,8,9 from the
    SilBioComp.pdf --R!!!


├── Code
│   ├── apply1.R: simple script showing how apply functions work in R
│   ├── apply2.R: simple script showing how user-defined functions can be called by apply
│   ├── basic_io.R: Simple script showing how to write/read csv
│   ├── boilerplate.R:simple boilerplate function in R
│   ├── break.R: showing how 'break' can be used to break out of for loops
│   ├── browse.R: Showing how browser can be entered to debug functions
│   ├── control.R: simple script showing how control statements work in R
│   ├── DataWrang.R: script demonstrating data wrangling capabilities in base R and reshape2
│   ├── DataWrangTidy.R: script demonstrating data wrangling capabilities in base R and tidyr
│   ├── get_treeHeight.R: As TreeHeight.R, but takes input csv argument from command line
│   ├── GPDD_maps.R: Generates world map plot using the data from GPDDFiltered.RData
│   ├── next.R: shows how 'next' can be used to modulate for loops
│   ├── PP_Lattice.R: Creates 3 graphs in lattice from the EcolArchivesData, outputs in Results. It also calculates the mean and median for each and outputs these to Results/PP_Results.csv.
│   ├── PP_Regress_loc.R: As PP_Regress.R, but subtypes by Feeding interaction, predator lifestage, location
│   ├── PP_Regress.R: Creates a faceted graph with regressions for EcolArchivesData by Type of feeding interaction and Predator lifestage. Outputs table of linear model information in Results.
│   ├── run_get_treeHeight.sh: bash script that runs get_treeHeight.R
│   ├── sample.R: Simulates population sampling in R
│   ├── TAutoCorr.R: Script finding correlation between temperature and succesive years in Key West Temperature Data
│   ├── TAutoCorr.tex: .tex code for interpretation of the results of TAutoCorr.R
│   ├── TreeHeight.R: Calcuates tree heights from distance/ange of tree.csv, outputs in Results/treeHts.csv
│   ├── try.R: Runs a population sampling script, demonstrating how 'try' can be used to prevent loops from breaking
│   ├── Vectorize1.R: Example of vectorization using sum()
│   └── Vectorize2.R Vectorization of a stochastic Ricker simulation
├── Data
│   ├── EcolArchives-E089-51-D1.csv: Data used for PP_Regress.R/PP_lattice.R/PP_regress_loc.R
│   ├── GPDDFiltered.RData: data used for gpdd_maps.R
│   ├── KeyWestAnnualMeanTemperature.RData: data used for TAutoCorr.R
│   ├── PoundHillData.csv: Data used by DataWrang/DataWrangTidy
│   ├── PoundHillMetaData.csv: Data used by DataWrang/DataWrangTidy
│   └── trees.csv: Data used by get_treeHeights.R

