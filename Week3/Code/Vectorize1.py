__author__ =  'Jack Rowntree jsr217@ic.acuk'
__version__ = '0.0.1' 
"""This script shows how vectorization can be used to save time, escpecially 
when running many iterations over large datasets"""
###########################################################################
import numpy as np
import sys
arr=np.random.rand(1000,1000)
#matrix of 1000^2 uniform distribution sample	
def vect1(arr=np.random.rand(1000,1000)):
	#matrix of 1000^2 uniform distribution sample
	Tot=0 #empty holder for sum values
	for i0 in xrange(0, arr.shape[0]): #for i all rows
		for i1 in xrange(0, arr.shape[1]): #for i in all columns
			Tot=Tot+arr[i0][i1] #add element to Tot
	print Tot

def speedy1(arr=np.random.rand(1000,1000)):
	#matrix of 1000^2 uniform distribution sample
	print arr.sum()
	
def main(argv):
	vect1(arr)
	speedy1(arr)
	
if (__name__ == "__main__"): #makes sure the "main" function is called from commandline
		status = main(sys.argv)
		sys.exit(status)
		

#~ print(system.time(SumAllElements(M)))#show  elapsed time running functions 
#~ print(system.time(sum(M)))
