__author__ =  'Jack Rowntree jsr217@ic.acuk'
__version__ = '0.0.1' 
"""This script runs the stochastic (with gaussian fluctuations) Ricker Eqn as an example
of vectorization, and how python can be faster than R"""



import sys
import math
import numpy as np
def stochrickvect(p0=np.random.uniform(.5,1.5,1000), r=1.2, K=1, sigma=0.2, numyears=100):
	N=np.zeros((numyears, len(p0)))#empty matrix w/ row for each year, column for each pop 
	N[0]=p0 #set initial pop as runif p0
	for yr in range(1, numyears): ##for each other pop, loop through the years
		fluct=np.random.normal(0, sigma, len(p0))#generate normally distributed vector of random numbers using function arguments
		N[yr,]=N[yr-1,]*np.exp(r*(1-N[yr-1,]/K)+fluct)##insert output into matrx
	print N

def main(argv):
	stochrickvect()

if (__name__ == "__main__"): #makes sure the "main" function is called from commandline
		status = main(sys.argv)
		sys.exit(status)
	#~ print("Stochastic Ricker takes:")
#~ print(system.time(res2<-stochrick()))	
	
