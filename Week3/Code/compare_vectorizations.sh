#!/bin/bash
#Author: 'Jack Rowntree jsr217@ic.ac.uk'
#Version = '0.0.1' 
#Times the 4 Vectorization scripts(2 python, 2 R)
echo -e "Vectorize1.py run time:"
time python Vectorize1.py
echo -e "##############\nVectorize2.py run time:"
time python Vectorize2.py
echo -e "##############\nVectorize1.R run time:"
time Rscript Vectorize1.R
echo -e "##############\nVectorize2.R run time:"
time Rscript Vectorize2.R
