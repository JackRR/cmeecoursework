R is about to run

R version 3.3.2 (2016-10-31) -- "Sincere Pumpkin Patch"
Copyright (C) 2016 The R Foundation for Statistical Computing
Platform: x86_64-pc-linux-gnu (64-bit)

R is free software and comes with ABSOLUTELY NO WARRANTY.
You are welcome to redistribute it under certain conditions.
Type 'license()' or 'licence()' for distribution details.

  Natural language support but running in an English locale

R is a collaborative project with many contributors.
Type 'contributors()' for more information and
'citation()' on how to cite R or R packages in publications.

Type 'demo()' for some demos, 'help()' for on-line help, or
'help.start()' for an HTML browser interface to help.
Type 'q()' to quit R.

> #!/usr/bin/env Rscript
> #Author: Jack Rowntree jsr217@ic.ac.uk
> #Version: 0.0.1 
> ###This RScript file contains all the code required for my simulations on the HPC cluster.
> 
> rm(list=ls())
> graphics.off()
> 
> #this function finds the number uses unique() to remove duplicates in the community, and returns the quantity of species types
> species_richness<-function(community){
+   richness=length(unique(community))
+   richness 
+ }
> ##########################################################################
> 
> ##########################################################################
> #this function initialises a sequence of integers from 1 to the argument 'size'. Representing maximal species richness
> initialise_max<-function(size){
+   species<-seq(size)
+   species
+ }
> ##########################################################################
> 
> ##########################################################################
> #this fuction initialises a vector of ones of length 'size' argument. Representing minmal species richness
> initialise_min<-function(size){
+   species<-replicate(size, 1)
+   species
+ }
> ##########################################################################
> 
> ##########################################################################
> #This function outputs two random numbers from a uniform distribution of integers between 1 and x
> choose_two<-function(x){
+   
+   #uniform distribution length x
+   list<-seq(1,x)
+   
+   #randomly sample 2 integers from the list, wthout replacement
+   sample(list, 2, replace=FALSE)
+ }
> ##########################################################################
> 
> ##########################################################################
> #This function represents one instance of birth and death in a community under the neutral model
> 
> #input argument- community vector
> neutral_step<-function(community){
+   #run choose_two() to give two random indices of the community vector
+   index<-choose_two(length(community))
+   #the member of the community at index 1 is replaced by the individual at index 2
+   community[index[1]]<-community[index[2]]
+   community
+ }
> ##########################################################################
> 
> ##########################################################################
> #this function represents one generation in the neutral model, which we define as size of community/2 birth/death steps
> 
> #input argument- community vector
> neutral_generation<-function(community){
+   x<-length(community)
+   
+   #if x is odd, amount of steps in generation rounded up using ceiling(). Run ceiling(x/2) neutral steps
+   for (i in 1:ceiling(x/2)){
+     community<-neutral_step(community)
+   }
+   community
+ }
> ##########################################################################
> 
> ##########################################################################
> #this function runs n=duration generations in a neutral theory model, and outputs a vector of species richness at each generation
> 
> #arguments: initial community, number of generations
> neutral_time_series<-function(initial, duration){
+   
+   #initialise vector, first element initial species richness
+   x<-numeric()
+   x[1]<-species_richness(initial)
+   
+   #initialise list, first element initial community
+   time_series<-list()
+   time_series[[1]]<-initial
+   
+   #duration argument sets number of gens by capping the for loop
+   for (i in 1:duration){
+     
+     #run a generation of the model and store in list
+     time_series[[i+1]]<-neutral_generation(time_series[[i]])
+     
+     #take species richness at this gen
+     x[i+1]<-species_richness(time_series[[i+1]])#
+     
+   }
+   x
+ }
> ##########################################################################
> 
> ##########################################################################
> 
> 
> ##########################################################################
> 
> ##########################################################################
> 
> #This function represents one birth/death instance in a neutral model with speciation. Therefore there is now a probability of a new species arising in the step, defined by v.
> neutral_step_speciation<-function(community, v){#v=speciation rate
+   #new species identifier defined as the largest integer in the community + 1 to prevent repetition
+   new_species<-max(community)+1
+   #randomly draws number between 1 and 0 from uniform distribution
+   probs<-runif(1,0,1)
+   #choose_two() produces two random indices of the community
+   index<-choose_two(length(community))
+   #if random number>speciation rate(more likely at low v), same process as neutral step without speciation
+   if (probs>v){
+     community[index[1]]<-community[index[2]]
+   }
+   #if random number< speciation rate(therefore less likely at lower v), member of community assigned new    identifier- new species!. 
+   if(probs<v){
+     community[index[1]]<-new_species
+   }
+   community
+ }
> ##########################################################################
> 
> ##########################################################################
> #this function represents one generation in the neutral model with speciation, which we define as size of community/2 birth/death steps
> 
> #input initial state and speciation rate
> neutral_generation_speciation<-function(community, v){
+   x<-length(community)
+   
+   #if x is odd, amount of steps in generation is rounded up using ceiling(). Run calculated number of     generations
+   for (i in 1:ceiling(x/2)){
+     community<-neutral_step_speciation(community,v)
+   }
+   community
+ }
> ##########################################################################
> 
> ##########################################################################
> #this function runs n=duration generations in a neutral theory model with speciation, and outputs a vector of species richness at each generation
> neutral_time_series_speciation<-function(community, v, duration){
+   
+   #initialise vector, first value= inital species richness  
+   x<-numeric()
+   x[1]<-species_richness(community)
+   
+   #initialise list, first element initial community vector
+   time_series<-list()
+   time_series[[1]]<-community
+   
+   #duration argument sets number of gens by capping the for loop
+   for (i in 1:duration){
+     
+     #run a generation of the model with speciation rate of argument v and store in list
+     time_series[[i+1]]<-neutral_generation_speciation(time_series[[i]],v)
+     
+     #take species richness at this gen
+     x[i+1]<-species_richness(time_series[[i]])
+   }
+   x
+   
+ }
> 
> 
> 
> 
> ##########################################################################
> 
> ##########################################################################
> #This function return species abundance distribution of input community 
> species_abundance<-function(community){
+   
+   #initialise empty vector
+   abundances<-numeric()
+   
+   #remove replicated taxa in community
+   values<-unique(community)
+   
+   #for each taxon, count how many there are in community and append to vector
+   for(i in 1:length(values)){
+     abundances[i]<-length(community[community==values[i]])
+   }
+   
+   #Rearrange abundances in descending order, print
+   return(sort(abundances, decreasing=TRUE))
+ } 
> 
> ##########################################################################
> 
> ##########################################################################
> #This function takes a vector of species abundances, and returns a vector of bins of increasing powers of 2, where each value is the frequency of this bin in the abundance vector.
> 
> #input is a vector of species abudances
> octave_classes <- function(abundances){
+   
+   #create vector of rounded down log2(abundances) + 1, meaning that values between the 2^x values will be assigned to the lower 2^x bin.
+   log_abundances <- floor(log(abundances,2))+1
+   
+   #tabulate() counts the frequency of log_abundances in integer bins starting at 1
+   octaves_abund <- tabulate(log_abundances)
+   
+   #print frequencies
+   return(octaves_abund)
+   
+ }
> 
> ##########################################################################
> 
> ##########################################################################
> #This function adds two vectors. If they are of unequal length, 0s are appended onto the shortest vector to allow vector addition.
> 
> sum_vect<-function(x,y){
+   z<-length(x)
+   b<-length(y)
+   newvect<-numeric()
+   #if vector lengths are equal, normal vector addition can take place
+   if (z==b){
+     newvect<-x+y}
+   #otherwise the shorter vector is appended with 0s to make its length equal
+   if(z<b){
+     numzeros<-b-z
+     x<-c(x,rep(0,numzeros))
+     newvect<-x+y
+   }
+   if(b<z){
+     numzeros<-z-b
+     y<-c(y,rep(0,numzeros))
+     newvect<-x+y
+   }
+   #print
+   newvect
+ }
> ##########################################################################
> 
> ##########################################################################
> #The cluster run function runs the neutral model with speciation, sampling species richness and abundance at chosen intervals. It is written to be time limited. It saves all relevant data from the simulation
> 
> 
> cluster_run <- function(speciation_rate, size, wall_time, interval_rich, interval_oct, burn_in_generations, output_file_name){
+   
+   #initial community, of size 'size'
+   generations <- initialise_min(size)
+   
+   #counter
+   i <- 1
+   
+   #storage objects for species abundance/richness data
+   SpRichBurn <- c()
+   oct <- list()
+   
+   #set start time
+   ptm <- proc.time()
+   
+   #only run simulation if elapsed time <= input time in seconds
+   while ((proc.time()[3] - ptm[3]) <= (60*wall_time)){
+     
+     #run neutral model generation
+     generations <- neutral_generation_speciation(generations, speciation_rate)
+     
+     #sample species richness and abundance at given intervals
+     if (i %in% seq(0,burn_in_generations,interval_rich)){
+       SpRichBurn[i/interval_rich] <- species_richness(generations)
+     }
+     if (i %% interval_oct == 0){
+       oct[i/interval_oct] <- list(octave_classes(species_abundance(generations))) 
+     }
+     #counter
+     i = i + 1
+   }
+   #update time
+   time <- proc.time()[3] - ptm[3]
+   
+   #save relevant data to file
+   save(SpRichBurn, oct, comm, time, speciation_rate, size, wall_time, interval_rich, interval_oct, burn_in_generations, file = output_file_name)
+ }
> 
> 
> ##########################################################################
> 
> ##########################################################################
> 
> #reading in PBS array index allows modulation of each parralel run of the R script.
> iter<-as.numeric(Sys.getenv("PBS_ARRAY_INDEX"))
> 
> #this code ensures that the 4 initial populations are divided equally among the 100 cluster runs
> if (iter %in% seq(1, 100, 4)){
+   J <- 500
+ } else if (iter %in% seq(2, 100, 4)){
+   J <- 1000
+ } else if (iter %in% seq(3, 100, 4)){
+   J <- 2500
+ } else if (iter %in% seq(4, 100, 4)){
+   J <- 5000
+ }
> 
> spec_rate <- 0.05579
> file_name <- paste0("neutral_sim_", iter, ".rda")
> 
> #setting seed as PBS array index here is vital to running each simulation randomly
> set.seed(iter)  
> cluster_run(spec_rate, J, wall_time = (11.5*60), 1, (J/10),(8*J), file_name)
> 
> 
R has finished running
 
Imperial College London HPC Service
-----------------------------------
Job hpc_cluster.sh, jobid 1008843[61].cx1, username jsr217 - end of execution at 06:53:53 Tue 12/12/17 on system cx1-50-3-16
