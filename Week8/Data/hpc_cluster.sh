#!/bin/bash
#PBS -l walltime=12:00:00
#PBS -l select=1:ncpus=1:mem=1gb
module load R
module load intel-suite
module load anaconda3/personal
anaconda- setup
conda install r
echo "R is about to run"
R --vanilla < $WORK/hpc_cluster_code.R
mv neutral_sim_* $WORK
echo "R has finished running"
