Week 4 CMEE Coursework 2017/2018: StatswithSparrows Week

├── Code
│   ├── Rstats1_2.R: Covers R basics, probability distributions, basic plotting in base R
│   ├── Rstats15.R: Covers multiple linear modelling, interactions in linear models
│   ├── Rstats3_4_5: covers t test, hypothesis testing, standard errors
│   └── Stats6_7_8_9_10.R: covers basics of linear modelling using Tarsus Length data.
├── Data
│   └── SparrowSize.txt: data used across the Week 4 Notes

