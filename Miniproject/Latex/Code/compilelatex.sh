
#!/bin/bash
# Author: Jack Rowntree JSR217@ic.ac.uk
# Script: compilelatex.sh
# Desc: compile .tex file and output pdf complete with bibliography, open in evince
# Arguments: .tex file name
# Date: Oct 2017
#!/bin/bash
pdflatex $1.tex
pdflatex $1.tex
bibtex $1
pdflatex $1.tex
pdflatex $1.tex
evince $1.pdf &
##remove extraneous file types
rm *~
rm *.aux
rm *.dvi
rm *.log
rm *.nav
rm *.out
rm *.snm
rm *.toc
