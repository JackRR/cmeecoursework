#README#
This directory contains all resources necessary to run my miniproject analyses and writeup. Organised like so:

-Code
--json_parse.py: Python script in which data is extracted and parsed into dataframe before being read by Analysis.R
--Analysis.R: R script in which data wrangling and analysis takes place
--pyenchant.py: Python script in which the spellchecking module enchant counts the number of spelling mistakes in the corpus
--run_Miniproject.sh: runs the first two scripts, the third optionally, and compiles the latex document.
-Data
--reviews_Patio_Lawn_and_Garden_5.json.gz: compressed json data to be parsed by json_parse.py
--reviewdata.csv: produced by json_parse.py, complete revew data, read by Anaysis.R
--pyenchantinput.csv: produced by Analysis.R, dataframe of all words in corpus, read by pyenchant.py

-Results
--sentplot.png: plot of sentiment vs Number of stars produced by Analysis.R
--countplot.png: plot of count vs Number of stars produced by Analysis.R
--.gitignore: otherwise empty upon sumbission
-Latex
--Plots
---sentplot.png
---countplot.png
--Code
---main.tex: tex file containing complete writeup
---mybib.bib: .bib file containin references
---compilelatex.sh: compiles the .tex file and produces pdf


NB: PACKAGES USED.
Python: enchant spellchecking module used. Just conda install enchant. Also uses collections, which I believe is standard, and pandas, which you probably have.
R: Tidyverse packages used extensively: readr (fast csv reading), dplyr(wrangling!), tidytext(some useful functions dealing with natural language data), tidyr(wrangling!), ggplot2 (mmmm... workflow). Just install any you don't have. Also used xtable for outputting some latex tables. MASS package used for ordinal logistic regression. Caret package used for cross validation procedure. You may have to install those last two.

Versions: R 3.2.3, Python 3.6.3
