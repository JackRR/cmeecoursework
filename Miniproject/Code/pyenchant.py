#!/usr/bin/env python
"""Runs a spellchecking script on the whole corpus and prints
the result"""
__author__ = 'Jack Rowntree jsr217@ic.ac.uk'
__version__ = '0.0.1'



import sys
import enchant
import pandas as pd
from collections import Counter
print("this will take about 40 seconds. perfect time to check a few emails :)")
d = enchant.Dict("en_US")
input=pd.read_csv("../Data/pyenchantinput.csv")

#the following chunk is how I would convert spelling mistakes if I wanted the code to run for longer, but there's not really any need

#this function returns boolean word wrongness value and its most likely intended spelling. Handles exception that occurs when there is no suggestion
# def count_replace(word):
#     try:
#         replace = d.suggest(word)[0]
#         count = d.check(word)
#         return replace, count
#     except IndexError:
#         return "nosuggestion"


#The following runs count_replace on the whole corpus and stores the values, with some lazy hadling of fuckups by just keeping the problem word
# spellchecked=[]
# errs=[]
# i=1
# for word in list(input['word']):
#     try:
#         suggest, err =count_replace(word)
#         spellchecked.append(suggest)
#         errs.append(err)
#     except ValueError:
#         #numeric case
#         spellchecked.append(word)
def main(argv):
    truth=[]
    for word in list(input['word']):
        try:
            tru=d.check(word)
            truth.append(tru)
        #to lazily catch funny characters, which won't match lexica anyway
        except:
            truth.append(False)
        
    output=(Counter(truth)[0]/Counter(truth)[1])
    print("ratio of misspelled words is", output)

if(__name__ == "__main__"):
	status = main(sys.argv)
	sys.exit(status)
