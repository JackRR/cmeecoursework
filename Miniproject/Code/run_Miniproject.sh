#!/bin/bash
# Author: Jack Rowntree JSR217@ic.ac.uk
# Desc: Runs complete miniproject
# Date: March 2018

#extracts data
python json_parse.py

#runs analyses
Rscript  Analysis.R

#optional little python script and compile latex.
read -p "Run pyenchant script? All it does is identify quantity of misspelled words in corpus, and takes about 1m30 (y/n)? " answer
case ${answer:0:1} in
    y|Y )
        python pyenchant.py
	cd ../Latex/Code
	bash compilelatex.sh main
    ;;
    * )
        cd ../Latex/Code
	bash compilelatex.sh main
    ;;
esac



